export class CreateCustomerDto {
  name: string;
  age: number;
  tel: number;
  gender: string;
}
